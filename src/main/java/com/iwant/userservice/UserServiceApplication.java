package com.iwant.userservice;

import com.iwant.userservice.data.models.security.SecurityConfig;
import com.iwant.userservice.data.models.security.SystemUser;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.TimeZone;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.Banner.Mode;
import org.springframework.boot.ResourceBanner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;

import com.iwant.userservice.data.models.CountryModel;
import com.iwant.userservice.services.interfaces.CountryService;
import com.iwant.userservice.services.interfaces.SecurityConfigService;
import com.iwant.userservice.services.interfaces.SystemUserService;
import com.iwant.userservice.utils.CommonHelper;

import org.codehaus.jackson.map.ObjectMapper;

@SpringBootApplication
public class UserServiceApplication { //implements CommandLineRunner {
	
	private static final Logger logger = LoggerFactory.getLogger(UserServiceApplication.class);

	public static int filterIndex;
	@Autowired
	private static Environment environment;
	public static Hashtable<String,SecurityConfig> configContext = new Hashtable();
	public static void main(String[] args) {
		logger.info("CurrentDate:"+CommonHelper.getCurrentTimestamp("Asia/Calcutta"));
		
		ZonedDateTime now = ZonedDateTime.now(ZoneOffset.of("IST", ZoneId.SHORT_IDS));
		ZonedDateTime now1 = ZonedDateTime.now(ZoneOffset.of("+05:30"));
		ZonedDateTime now2 = ZonedDateTime.now(ZoneOffset.ofHoursMinutes(5,30));
		Date d = Date.from(now.toInstant());
		logger.info("Time:"+now);
		logger.info("Time:"+now1);
		logger.info("Time:"+now2);
		
		SpringApplication app = new SpringApplication(UserServiceApplication.class); 
        app.setBannerMode(Mode.CONSOLE);
        ApplicationContext context = app.run(args);
		/***
		 * Step 1: Default security configuration
		 * */
        SecurityConfigService configManager = context.getBean(SecurityConfigService.class);
		SecurityConfig defaultConfig = configManager.findByDomain("Default");
		if(defaultConfig == null) {
			defaultConfig = new SecurityConfig();
			defaultConfig.setDomain("Default");
			defaultConfig.setMaxAttampts(3);
			defaultConfig.setPasswordValidityInDays(365);
			SecurityConfig.TokenConfig tConfig = defaultConfig.tokenConfig("Any", 180,60*60*24*180);
			defaultConfig.setDefaultTokenConfig(tConfig);
			defaultConfig.setOtpLength(4);
			defaultConfig.setOtpValidityinSeconds(60);
			SecurityConfig.OtpDeliveryMode sms = defaultConfig.otpDeliveryMode(1, "SMS");
			SecurityConfig.OtpDeliveryMode email = defaultConfig.otpDeliveryMode(2, "EMAIL");
			Collection<SecurityConfig.OtpDeliveryMode> otpDeliveryConfig = new Vector();
			otpDeliveryConfig.add(sms);
			otpDeliveryConfig.add(email);
			defaultConfig.setOtpDeliveryConfig(otpDeliveryConfig);
			defaultConfig.setDefaultTimezone("+05:30");
			
			defaultConfig = configManager.create(defaultConfig);
			
			logger.info("Added default security Config successfully:"+defaultConfig.getId());
		}else {
			logger.info("Default Security Config already exists");
		}
		configContext.put("Default", defaultConfig);
		
		/***
		 * Step 2: Create Super Admin User
		 * */
		
		SystemUserService userService = context.getBean(SystemUserService.class);
		long totalUsers = userService.getUserCount();
		logger.info(totalUsers + " Users found");
		if(totalUsers<=0) {
			SystemUser adminUser = new SystemUser();
			adminUser.setDomain("Default");
			adminUser.setUserType(1);
			adminUser.setFirstName("System Administrator");
			adminUser.setLastName("System Generated User");
			adminUser.setUsername("admin");
			adminUser.setExternalReferenceId("admin@domain.com");
			adminUser.setPassword("iWant@321");
			adminUser.setEncryptionType(0);
			adminUser.setMobile("9879275055");
			adminUser.setEmail("timir.patel@iwantunlimited.com");
			adminUser.setAuditInfo("Created on Bootstrap");
			adminUser.setTimeZone("Asia/Culcutta");
			adminUser.setLanguage("English");
			adminUser.setCreatedBy("System Bootstrap");
			List<String> authorities = new ArrayList();
			authorities.add("SYSTEM_ADMIN");
			adminUser.setAuthorities(authorities);
			adminUser = userService.create(adminUser);
			
			logger.info(adminUser.getId()+": Admin User Created Successfully");
		}
		SystemUser user = userService.findByIdentity("timir.patel@iwantunlimited.com", "Default", 1);
		logger.info(user.getUsername()+":"+user.getId());
	}
		
}
