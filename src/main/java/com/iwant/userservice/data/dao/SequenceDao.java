package com.iwant.userservice.data.dao;

import com.iwant.userservice.exceptions.SequenceException;

public interface SequenceDao {
	
	long getNextSequenceId(String key) throws SequenceException;
	
}
