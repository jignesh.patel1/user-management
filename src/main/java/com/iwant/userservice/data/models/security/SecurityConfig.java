package com.iwant.userservice.data.models.security;

import java.util.Collection;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("security-config")
public class SecurityConfig {

	@Id
	private String id;
	
	@Indexed
	private String domain;
	
	private String passwordFormat;
	private int encryptionType;
	private int maxAttampts;
	private int passwordValidityInDays;
	private int defaultTokenValidityInSeconds;
	private TokenConfig defaultTokenConfig;
	private Collection<TokenConfig> tokenConfig;
	private int otpLength;
	private int otpValidityinSeconds;
	private int otpEncryption;
	private Collection<OtpDeliveryMode> otpDeliveryConfig;
	private String defaultTimezone;
	
	public class TokenConfig{
		String deviceType = "Any";
		int tokenValidityInSeconds = 180;
		int refreshTokenValidityInSeconds = 60*60*24*180;
	}
	public class OtpDeliveryMode{
		int id;
		String alias;
	}
	public OtpDeliveryMode otpDeliveryMode(int id,String alias) {
		OtpDeliveryMode mode = new OtpDeliveryMode();
		mode.id = id;
		mode.alias =alias;
		return mode;
	}
	public TokenConfig tokenConfig(String device, int validity, int refreshTokenValidity) {
		TokenConfig config = new TokenConfig();
		config.deviceType = device;
		config.tokenValidityInSeconds = validity;
		config.refreshTokenValidityInSeconds = refreshTokenValidity;
		return config;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getPasswordFormat() {
		return passwordFormat;
	}
	public void setPasswordFormat(String passwordFormat) {
		this.passwordFormat = passwordFormat;
	}
	public int getMaxAttampts() {
		return maxAttampts;
	}
	public void setMaxAttampts(int maxAttampts) {
		this.maxAttampts = maxAttampts;
	}
	public int getPasswordValidityInDays() {
		return passwordValidityInDays;
	}
	public void setPasswordValidityInDays(int passwordValidityInDays) {
		this.passwordValidityInDays = passwordValidityInDays;
	}
	public int getDefaultTokenValidityInSeconds() {
		return defaultTokenValidityInSeconds;
	}
	public void setDefaultTokenValidityInSeconds(int defaultTokenValidityInSeconds) {
		this.defaultTokenValidityInSeconds = defaultTokenValidityInSeconds;
	}
	public Collection<TokenConfig> getTokenConfig() {
		return tokenConfig;
	}
	public void setTokenConfig(Collection<TokenConfig> tokenConfig) {
		this.tokenConfig = tokenConfig;
	}
	public TokenConfig getDefaultTokenConfig() {
		return defaultTokenConfig;
	}
	public void setDefaultTokenConfig(TokenConfig defaultTokenConfig) {
		this.defaultTokenConfig = defaultTokenConfig;
	}
	public int getOtpLength() {
		return otpLength;
	}
	public void setOtpLength(int otpLength) {
		this.otpLength = otpLength;
	}
	public int getOtpValidityinSeconds() {
		return otpValidityinSeconds;
	}
	public void setOtpValidityinSeconds(int otpValidityinSeconds) {
		this.otpValidityinSeconds = otpValidityinSeconds;
	}
	public Collection<OtpDeliveryMode> getOtpDeliveryConfig() {
		return otpDeliveryConfig;
	}
	public void setOtpDeliveryConfig(Collection<OtpDeliveryMode> otpDeliveryConfig) {
		this.otpDeliveryConfig = otpDeliveryConfig;
	}
	public String getDefaultTimezone() {
		return defaultTimezone;
	}
	public void setDefaultTimezone(String defaultTimezone) {
		this.defaultTimezone = defaultTimezone;
	}
	public int getEncryptionType() {
		return encryptionType;
	}
	public void setEncryptionType(int encryptionType) {
		this.encryptionType = encryptionType;
	}
	public int getOtpEncryption() {
		return otpEncryption;
	}
	public void setOtpEncryption(int otpEncryption) {
		this.otpEncryption = otpEncryption;
	}
}
