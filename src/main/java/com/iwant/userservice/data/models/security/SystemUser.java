package com.iwant.userservice.data.models.security;

import java.util.Date;
import java.util.List;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("system-user")
public class SystemUser {

	private static final Logger logger = LoggerFactory.getLogger(SystemUser.class);
	
	@Id
	private String id;
	private String domain;
	private int userType;
	private String firstName;
	private String lastName;
	private String username;
	private String password;
	private int encryptionType;
	
	private String mobile;
	private String mcc;
	private String email;
	private String externalReferenceId;
	
	private boolean isBlocked;
	private boolean isDeleted;
	
	private boolean is2FA;
	private String secret;
	private int codeType2FA;
	private String language;
	private boolean forcePasswordChange;
	private String timeZone;
	private Date passwordExpiry;
	private int passwordValidityDays;
	private String region;
	private Date lastLoginOn;
	private Date createdOn;
	private Date updatedOn;
	
	private String createdBy;
	private String updatedBy;
	private String auditInfo;
	private List<String> authorities;
	
	public String getId() {
		return id;
	}
	
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getEncryptionType() {
		return encryptionType;
	}
	public void setEncryptionType(int encryptionType) {
		this.encryptionType = encryptionType;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getMcc() {
		return mcc;
	}
	public void setMcc(String mcc) {
		this.mcc = mcc;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getExternalReferenceId() {
		return externalReferenceId;
	}
	public void setExternalReferenceId(String externalReferenceId) {
		this.externalReferenceId = externalReferenceId;
	}
	public boolean isBlocked() {
		return isBlocked;
	}
	public void setBlocked(boolean isBlocked) {
		this.isBlocked = isBlocked;
	}
	public boolean isDeleted() {
		return isDeleted;
	}
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public boolean isIs2FA() {
		return is2FA;
	}
	public void setIs2FA(boolean is2fa) {
		this.is2FA = is2fa;
		/*
		if(is2FA) {
			this.secret = Base32.random();
			logger.trace("2FA enabled for user:"+this.userName);
			logger.trace("2FA activated ["+this.userName+"] with secret:- "+secret);
			
		}*/
	}
	public String getSecret() {
		return secret;
	}
	public void setSecret(String secret) {
		this.secret = secret;
	}
	public int getCodeType2FA() {
		return codeType2FA;
	}
	public void setCodeType2FA(int codeType2FA) {
		this.codeType2FA = codeType2FA;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public boolean isForcePasswordChange() {
		return forcePasswordChange;
	}
	public void setForcePasswordChange(boolean forcePasswordChange) {
		this.forcePasswordChange = forcePasswordChange;
	}
	public String getTimeZone() {
		return timeZone;
	}
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	public Date getPasswordExpiry() {
		return passwordExpiry;
	}
	public void setPasswordExpiry(Date passwordExpiry) {
		this.passwordExpiry = passwordExpiry;
	}
	public int getPasswordValidityDays() {
		return passwordValidityDays;
	}
	public void setPasswordValidityDays(int passwordValidityDays) {
		this.passwordValidityDays = passwordValidityDays;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public Date getLastLoginOn() {
		return lastLoginOn;
	}
	public void setLastLoginOn(Date lastLoginOn) {
		this.lastLoginOn = lastLoginOn;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getAuditInfo() {
		return auditInfo;
	}
	public void setAuditInfo(String auditInfo) {
		this.auditInfo = auditInfo;
	}
	public List<String> getAuthorities() {
		return authorities;
	}
	public void setAuthorities(List<String> authorities) {
		this.authorities = authorities;
	}

	public int getUserType() {
		return userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}
	
}
