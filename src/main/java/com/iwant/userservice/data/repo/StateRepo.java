package com.iwant.userservice.data.repo;

import org.springframework.data.mongodb.repository.MongoRepository;


import com.iwant.userservice.data.models.StateModel;

public interface StateRepo extends MongoRepository<StateModel,String>{

}
