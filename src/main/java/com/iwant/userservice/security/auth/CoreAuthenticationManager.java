package com.iwant.userservice.security.auth;

import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.iwant.userservice.UserServiceApplication;
import com.iwant.userservice.data.models.security.SecurityConfig;
import com.iwant.userservice.data.models.security.SystemUser;
import com.iwant.userservice.security.helper.AuthHelper;
import com.iwant.userservice.security.helper.PasswordAttemptSession;
import com.iwant.userservice.security.helper.PasswordAttemptTracker;
import com.iwant.userservice.services.interfaces.OtpService;
import com.iwant.userservice.services.interfaces.SystemUserService;
import com.iwant.userservice.wrapper.OtpValidation;

@Service
public class CoreAuthenticationManager implements AuthenticationManager{

	private static final Logger logger = LoggerFactory.getLogger(CoreAuthenticationManager.class);
	@Autowired
	SystemUserService userService;
	
	@Autowired
	PasswordEncoder encoder;
	
	@Autowired
	OtpService otpService;
	
	@Autowired
	PasswordAttemptTracker passwordAttemptContext;
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		logger.info("Started: authenticate()");
		String username = (String)authentication.getPrincipal();
		String password = (String)authentication.getCredentials();
		HashMap info = (HashMap)authentication.getDetails();
		String loginType = (String)info.get("passwordType");
		String userType = (String)info.get("accountType");
		String domain = (String)info.get("domain");
		String transactionCode = (String) info.get("transactionId");
		
		int passwordType = AuthHelper.validateLoginType(loginType);
		userType = AuthHelper.validateUserType(userType,info);
		transactionCode = AuthHelper.validateTransactionCode(transactionCode, info);
		
		if(domain==null || domain.length()<1) {
			logger.info("domain not present in authentication request, setting it to default: <Default>");
			domain = "Default";
		}
		
		SystemUser user = userService.findByIdentity(username.toLowerCase(), domain, Integer.valueOf(userType).intValue());
		
		if(user == null) {
			logger.info("Usernot found:" + username);
			throw new BadCredentialsException("Invalid Username or Password");
		}else {
			switch(passwordType) {
			case AuthHelper.LOGIN_TYPE_PASSWORD:
				SecurityConfig config = UserServiceApplication.configContext.get(domain);
				
				return authenticateByCredential(username,password,user,config.getMaxAttampts());
			case AuthHelper.LOGIN_TYPE_OTP:
				return authenticateByOtp(domain,transactionCode,username,password,user);
			default:
				throw new BadCredentialsException("Invalid Authentication Request");
				
			}
		}
		
	}
	
	public Authentication authenticateByCredential(String username, String password, SystemUser user,int maxAllowedAttempts) {
		
		
		boolean isPasswordNotExpired = false;
		boolean doesPasswordMatch = false;
		PasswordAttemptSession passwordAttemptSession = new PasswordAttemptSession(user.getId(),maxAllowedAttempts);
		passwordAttemptSession = passwordAttemptContext.createSession(user.getId(), passwordAttemptSession);
		
		Date timeStamp = new Date();
		Date expiry = user.getPasswordExpiry();
		isPasswordNotExpired = expiry.before(timeStamp);
		
		
		
		List<GrantedAuthority> authorities = new ArrayList();
		
		boolean forceChangePassword = isPasswordNotExpired? true : false;
		if(forceChangePassword) {
			GrantedAuthority role = new SimpleGrantedAuthority("ROLE_CHANGE_PASSWORD");
			authorities.add(role);
		}else {
			authorities =transformAuthorities(user.getAuthorities());
		}
		if(passwordAttemptSession.canAttempt()){
			passwordAttemptSession.newAttempt();
			switch(user.getEncryptionType()) {
			case 0:
				doesPasswordMatch = password.equals(user.getPassword());
				break;
			case 1:
				doesPasswordMatch = encoder.matches(password, user.getPassword());
				break;
			}
			
		}else {
			throw new BadCredentialsException("Maximum Password Attempt Limit exhausted");
		}
		
		if(doesPasswordMatch) {
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
				user.getUsername(),
				user.getPassword(),
				authorities
				);
			return token;
			
		}else {
			throw new BadCredentialsException("Invalid Username or Password");
		}
	}
	public Authentication authenticateByOtp(String domain, String transactionCode, String username, String password, SystemUser user){
		if(transactionCode==null || transactionCode.length()<1) {
			throw new BadCredentialsException("Invalid Request, Missing: <transactionCode>");
		}
		OtpValidation res = otpService.validateOtp(transactionCode, password, username, domain);
		
		if(res.isValid()) {
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
					user.getUsername(),
					user.getPassword(),
					transformAuthorities(user.getAuthorities())
					);
				return token;
		}else {
			throw new BadCredentialsException("Invalid Username or Password");
		}
		
	}
	public List<GrantedAuthority> transformAuthorities(List<String> authorities){
		ArrayList<GrantedAuthority> list = new ArrayList();
		for(String authority:authorities) {
			GrantedAuthority role = new SimpleGrantedAuthority(authority);
			list.add(role);
		}
		return list;
	}
	
}
