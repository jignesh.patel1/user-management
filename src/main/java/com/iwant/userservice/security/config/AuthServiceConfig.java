package com.iwant.userservice.security.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;

import com.iwant.userservice.security.helper.JsonToUrlEncodedAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled=true)
public class AuthServiceConfig extends WebSecurityConfigurerAdapter{
	
	private static final Logger logger = LoggerFactory.getLogger(AuthServiceConfig.class);
	
	@Autowired
    JsonToUrlEncodedAuthenticationFilter jsonFilter;
	
	
	@Bean
	public PasswordEncoder encoder() {
		return new BCryptPasswordEncoder();
		
	}
	
	
	public AuthServiceConfig() {
		super(false);
	}
	
	@Override
	protected void configure(HttpSecurity webSecurity) throws Exception{
		
		webSecurity
			.authorizeRequests().
				anyRequest().
				authenticated().
				and()
			.addFilterBefore(jsonFilter,ChannelProcessingFilter.class)
				.sessionManagement().
				sessionCreationPolicy(SessionCreationPolicy.NEVER);
		
		
		
		logger.trace("Security Config: Session Creation Updated to NEVER");
		
	}

	

}
