package com.iwant.userservice.security.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;

import com.iwant.userservice.security.auth.CoreAuthenticationManager;
import com.iwant.userservice.security.helper.JsonToUrlEncodedAuthenticationFilter;

@Configuration
@EnableAuthorizationServer
public class OAuth2Config extends AuthorizationServerConfigurerAdapter{
	
	private static final Logger logger = LoggerFactory.getLogger(OAuth2Config.class);
	
	@Autowired
	private CoreAuthenticationManager authenticationManager;
	

	@Value("${config.oauth2.client-id}")
	private String clientid;
	
	@Value("${config.oauth2.client-secret}")
	private String clientSecret;
	
	@Value("${config.oauth2.privateKey}")
	private String privateKey;
	
	@Value("${config.oauth2.publicKey}")
	private String publicKey;
	
	@Autowired
    JsonToUrlEncodedAuthenticationFilter jsonFilter;
	
	@Override
	public void configure(AuthorizationServerSecurityConfigurer security)throws Exception {
		
		security
		//	.allowFormAuthenticationForClients()
			//.tokenKeyAccess("permitAll()")
			.checkTokenAccess("permitAll()")
			
			;
		super.configure(security);
		logger.trace("OAuth2Config:Configure:AuthorizationServerSecurityConfigurer: Completed");
	}
	
	@Override
	public void configure(ClientDetailsServiceConfigurer clients)throws Exception {
		clients.inMemory().
		withClient(clientid).
			secret(clientSecret).
			scopes("Read","Write").
			authorizedGrantTypes("password","refresh_token","authorization_code").
		accessTokenValiditySeconds(3600).
		refreshTokenValiditySeconds(18000);
		logger.trace("OAuth2Config:ClientDetailsServiceConfigurer:Configured");
		
	}
	
	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endPoints)throws Exception{
			endPoints
			.authenticationManager(authenticationManager)
			.tokenStore(tokenStore())
			.accessTokenConverter(tokenEnhancer())
			;
			
		logger.trace("Authentication Manager Configured with Token Enhancer");
	}
	
	
	
	@Bean
	public JwtTokenStore tokenStore() {
		logger.trace("JwtTokenStore Created");
		return new JwtTokenStore(tokenEnhancer());
		
	}
	
	@Bean
	public JwtAccessTokenConverter tokenEnhancer() {
		JwtAccessTokenConverter converter = new CustomTokenEnhancer();
		converter.setSigningKey(privateKey);
		converter.setVerifierKey(publicKey);
		return converter;
	}

}
