package com.iwant.userservice.security.helper;

import java.util.HashMap;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class AuthHelper {
	
	private static final Logger logger = LoggerFactory.getLogger(AuthHelper.class);
	
	public static final int LOGIN_TYPE_PASSWORD=0;
	public static final int LOGIN_TYPE_OTP=1;
	

	public static String validateUserType(String userType,HashMap info) {
		
		if(userType==null || userType.length()<1) {
			userType = (String)info.get("usertype");
			if(userType==null || userType.length()<1) {
				userType = (String)info.get("UserType");
				if(userType==null || userType.length()<1) {
					userType = (String)info.get("loginType");
					if(userType==null || userType.length()<1) {
						userType = (String)info.get("LoginType");
						if(userType==null || userType.length()<1) {
							userType = (String)info.get("accountType");
						}else{
							logger.info("UserType not present in authentication request, setting it to default: 0");
							userType = "0";
						}
					}
				}
			}
		}
		try {
			
			int type = Integer.parseInt(userType);
			
		}catch(NumberFormatException e) {
			
			if(userType.equalsIgnoreCase("customer") || userType.toLowerCase().contains("customer")) {
				userType = "0";
			}
			if(userType.equalsIgnoreCase("student") || userType.toLowerCase().contains("user")) {
				userType = "0";
			}
			if(userType.equalsIgnoreCase("staff") || userType.toLowerCase().contains("staff")) {
				userType = "1";
			}
			if(userType.equalsIgnoreCase("backend") || userType.toLowerCase().contains("backend")) {
				userType = "1";
			}
			if(userType.equalsIgnoreCase("partner") || userType.toLowerCase().contains("partner")) {
				userType = "2";
			}
			if(userType.equalsIgnoreCase("seller") || userType.toLowerCase().contains("seller")) {
				userType = "2";
			}
		}
		return userType;
	}
	public static int validateLoginType(String loginType) {

		if(loginType==null || loginType.length()<1) {
			logger.info("loginType not present in authentication request, setting it to default: <password>");
			loginType = "password";
		}
		if(loginType.toLowerCase().contains("password"))
			return LOGIN_TYPE_PASSWORD;
		if(loginType.toLowerCase().contains("otp"))
			return LOGIN_TYPE_OTP;
		
		return LOGIN_TYPE_PASSWORD;
	}
	public static String validateTransactionCode(String transactionCode, HashMap info) {
		if(transactionCode==null || transactionCode.length()<1) {
			transactionCode = (String) info.get("transactionCode");
			if(transactionCode==null || transactionCode.length()<1) {
				transactionCode = (String) info.get("transactionId");
			}
		}
		return transactionCode;
	}
}
