package com.iwant.userservice.services;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.BasicUpdate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.iwant.userservice.data.models.CountryModel;
import com.iwant.userservice.data.models.SequenceId;
import com.iwant.userservice.services.common.CommonServices;
import com.iwant.userservice.services.interfaces.CountryService;
import com.iwant.userservice.utils.CommonHelper;

import lombok.NonNull;

@Service (value="countryService")
public class CountryServiceImpl implements CountryService {
	private static final Logger logger = LoggerFactory.getLogger(CountryServiceImpl.class);
	private static final String COUNTRY_SEQ_KEY = "COUNTRY";
	@Autowired
	MongoTemplate template;
	
	@Autowired
	CommonServices common;

	@Override
	public CountryModel createNewCountry(@NonNull CountryModel input) {
		// TODO Auto-generated method stub
		Query query = new BasicQuery("{}") ;
		long records = template.count(query, CountryModel.class);
		logger.info("Total "+records+ " Records Found");
		if(records<1) {
			logger.info("First-time insert in Country, Creating new Sequence");
			common.createNewSequence(COUNTRY_SEQ_KEY, input.getDomain());
		};
		long next = common.getNextSequence(COUNTRY_SEQ_KEY, input.getDomain());
		input.setCode(next);
		input.setCreatedOn(new Date());
		return template.insert(input);
		
	}

	@Override
	public boolean findAndDeleteById(String id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean findAndDeleteByCode(int code) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public CountryModel findAndUpdate(CountryModel input) {
		input.setUpdatedOn(new Date());
		return template.save(input);
	}

	@Override
	public CountryModel findOneById(String id) {
		Query query = new Query(Criteria.where("Id").is(id));
		CountryModel result = template.findOne(query, CountryModel.class);
		return result;
	}

	@Override
	public CountryModel findOneByCode(int code) {
		Query query = new Query(Criteria.where("code").is(code));
		return template.findOne(query, CountryModel.class);
	}

	@Override
	public List<CountryModel> findAllByDomain(String domain) {
		logger.info("findAllCountriesByDomain:"+domain);
		
		Query query = new Query(Criteria.where("domain").is(domain).and("isActive").is(true));
		
		return template.find(query, CountryModel.class);
	}
	
	
}
