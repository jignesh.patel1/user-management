package com.iwant.userservice.services;

import java.time.Duration;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.iwant.userservice.UserServiceApplication;
import com.iwant.userservice.data.models.security.OtpModel;
import com.iwant.userservice.data.models.security.SecurityConfig;
import com.iwant.userservice.services.interfaces.OtpService;
import com.iwant.userservice.utils.CodeGenerator;
import com.iwant.userservice.utils.CommonHelper;
import com.iwant.userservice.wrapper.OtpResponse;
import com.iwant.userservice.wrapper.OtpValidation;

@Service
public class OtpServiceImpl implements OtpService {
	
	private static final Logger logger = LoggerFactory.getLogger(OtpServiceImpl.class);
	
	@Autowired
	CodeGenerator codeGenerator;

	@Autowired
	PasswordEncoder encoder;
	
	@Autowired
	MongoTemplate mongoService;
	
	@Override
	public OtpResponse sendOtp(String domain,String transactionInfo,String mcc,String mobile,String email,Object custom) {
		logger.info(String.format("sendOtp(%s,%s,%s,%s,%s)",domain,transactionInfo,mcc,mobile,email));
		OtpModel otp = generateOtp(transactionInfo,domain);
		otp.setMcc(mcc);
		otp.setMobile(mobile);
		otp.setEmail(email);
		ZonedDateTime expiryZ = ZonedDateTime.ofInstant(otp.getExpiry().toInstant(), ZoneOffset.of(otp.getTimeZone()));
		logger.info("Otp will be saved with expiry date as: "+otp.getExpiry());
		logger.info("Otp will be saved with expiry date as: "+expiryZ);
		boolean bSent = dispatchOtp(domain,
				otp.getOtpCode(),
				otp.getTransactionCode(),
				mcc,
				mobile,
				email,
				custom
				);
		if(bSent) {
			otp = mongoService.insert(otp);
			if(otp!=null) {
				ZonedDateTime expiry = ZonedDateTime.ofInstant(otp.getExpiry().toInstant(), ZoneOffset.of(otp.getTimeZone()));
				logger.info("Otp Saved Successfully with expiry date: "+expiry);
				logger.info("Otp Saved Successfully with expiry date: "+otp.getExpiry());
				OtpResponse response = new OtpResponse();
				ZonedDateTime exp = ZonedDateTime.ofInstant(otp.getExpiry().toInstant(), ZoneOffset.of(otp.getTimeZone()));
				response.setTransactionId(otp.getId());
				response.setTransactionCode(otp.getTransactionCode());
				response.setValidityInSeconds(otp.getValidityInSeconds());
				response.setExpiry(exp);
				response.setTimeZoneId(otp.getTimeZone());
				return response;
			}else {
				return null;
			}
		}else {
			return null;
		}
		
	}
	
	
	@Override
	public OtpValidation validateOtp(String transactionId,String otp, String identity,String domain) {
		String queryString = String.format(
				"{ "
					+ "transactionCode : \"%s\","
					+ "domain : \"%s\","
					+ "$or : "
							+ "["
							+ "{mobile : \"%s\"},"
							+ "{email : \"%s\"}"
							+ "] "
				+ "}",
				transactionId,domain,identity,identity);
		logger.info(queryString);
		Query query = new BasicQuery(queryString);
		OtpModel model = mongoService.findOne(query, OtpModel.class);
		OtpValidation res = new OtpValidation();
		if(model!=null) {
			Date timeStamp = new Date();
			Date expiry = model.getExpiry();
			
			ZonedDateTime currentZ = ZonedDateTime.ofInstant(timeStamp.toInstant(), ZoneOffset.of(model.getTimeZone()));
			ZonedDateTime expiryZ = ZonedDateTime.ofInstant(expiry.toInstant(), ZoneOffset.of(model.getTimeZone()));
			logger.info("\n");
			logger.info("currentTime:\t"+currentZ);
			logger.info("currentTime:\t"+timeStamp);
			
			logger.info("\n");
			logger.info("expiry:\t"+expiryZ);
			logger.info("expiry:\t"+expiry);
			logger.info("\n");
			
			res.setDomain(model.getDomain());
			res.setTransactionCode(model.getTransactionCode());
			res.setUsername(identity);
			res.setOtpValue("*****");
			boolean isExpired = expiry.before(timeStamp);
			
			boolean doesMatch = false;
			switch(model.getEncryptionType()) {
				case 0:
					doesMatch = otp.equals(model.getOtp());
					break;
				case 1:
					doesMatch = encoder.matches(otp, model.getOtp());
			}
			logger.info("isExpired: [ "+isExpired+" ]\n");
			logger.info("doesMatch: [ "+doesMatch+" ]\n");
			boolean otpValidationResult = (!isExpired) && doesMatch;
			String info = otpValidationResult ? "OTP Verification Successfull" : ((doesMatch)? "OTP Expired":"Incorrect Otp");
			
			res.setValid(otpValidationResult);
			res.setValidationRespose(info);
			return res;
		}else {
			res.setDomain("Not Found");
			res.setDomain("Not Found");
			res.setValid(false);
			res.setTransactionCode("Not Found");
			res.setValidationRespose("Matching Otp Not Found in repository");
			return res;
		}
		
	}

	
	private OtpModel generateOtp(String transactionInfo,String domain) {
		SecurityConfig config = UserServiceApplication.configContext.get(domain);
		OtpModel model = new OtpModel();
		
		int otpLength = (config.getOtpLength()==0)? 6/*default length*/: config.getOtpLength();
		int otpValidity = (config.getOtpValidityinSeconds()==0)? 120 /*default validity*/ : config.getOtpValidityinSeconds();
		String msg = String.format("OtpLength:%d, otpValidity: %d", otpLength,otpValidity);
		logger.info(msg);
		String otp = CommonHelper.generateOtp(otpLength);
		logger.info("Otp:" + ((otp != "-1" )? otp /*to-do making */:"<not generated>"));
		model.setOtpCode(Integer.parseInt(otp));
		
		Calendar timer = Calendar.getInstance();
		Date timeStampwithoutZone = new Date(timer.getTimeInMillis());
		model.setCreatedOn(timeStampwithoutZone);
		
		model.setDomain(domain);
		model.setTransactionType(transactionInfo);
		
		String transactionCode = codeGenerator.generateAccessCode(Calendar.getInstance().getTimeInMillis());
		logger.info("transactionCode:"+transactionCode);
		model.setTransactionCode(transactionCode);
		
		model.setEncryptionType(config.getOtpEncryption());
		model.setOtp(encrypt(otp,config.getOtpEncryption()));
		timer.add(Calendar.SECOND, otpValidity);
		Date expiryDateWithoutTimeZone = new Date(timer.getTimeInMillis());
		model.setExpiry(expiryDateWithoutTimeZone);
		model.setTimeZone(config.getDefaultTimezone());
		
		ZonedDateTime otpExp = ZonedDateTime.ofInstant(expiryDateWithoutTimeZone.toInstant(), ZoneOffset.of(model.getTimeZone()));
		logger.info("Otp generated successfully with expiry date as: "+otpExp);
		logger.info("Otp generated successfully with expiry date as: "+model.getExpiry());
		return model;
	}
	private boolean dispatchOtp(
			String domain, 
			int otp,
			String transactionCode,
			String mcc, 
			String mobile,
			String email,
			Object custom) {
		
		// TODO get SMS template from Domain Config, transform it and send it.
		return true;
	}
	private String encrypt(String data,int mode) {
		switch(mode) {
		case 0:
			return data;
		case 1:
			return encoder.encode(data);
		}
		return data;
	}


	@Override
	public OtpResponse resendOtp(String domain, String transactionInfo, String mcc, String mobile, String email,
			Object custom) {
		// TODO Auto-generated method stub
		return null;
	}
}
