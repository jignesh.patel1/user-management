package com.iwant.userservice.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.iwant.userservice.data.models.security.SecurityConfig;
import com.iwant.userservice.data.models.security.SecurityConfig.TokenConfig;
import com.iwant.userservice.services.interfaces.SecurityConfigService;

@Service
public class SecurityConfigServiceImpl implements SecurityConfigService {
	
	@Autowired
	MongoTemplate mongoService;

	@Override
	public SecurityConfig findByDomain(String domain) {
		Query query = new Query(Criteria.where("domain").is(domain));
		return mongoService.findOne(query, SecurityConfig.class);
	}

	@Override
	public SecurityConfig create(SecurityConfig model) {
		// TODO Auto-generated method stub
		return mongoService.insert(model);
	}

	@Override
	public SecurityConfig update(SecurityConfig model) {
		// TODO Auto-generated method stub
		return mongoService.save(model);
	}

	@Override
	public TokenConfig findTokenConfigFor(String device) {
		// TODO Auto-generated method stub
		return null;
	}

}
