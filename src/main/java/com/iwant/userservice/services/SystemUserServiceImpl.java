package com.iwant.userservice.services;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.iwant.userservice.UserServiceApplication;
import com.iwant.userservice.data.models.security.SecurityConfig;
import com.iwant.userservice.data.models.security.SystemUser;
import com.iwant.userservice.services.interfaces.SystemUserService;

@Service
public class SystemUserServiceImpl implements SystemUserService {

	private static final Logger logger = LoggerFactory.getLogger(SystemUserServiceImpl.class);
			
	@Autowired
	MongoTemplate mongoService;
	
	@Override
	public SystemUser create(SystemUser user) {
		
		SecurityConfig config = UserServiceApplication.configContext.get(user.getDomain());
		logger.info(config.getDomain()+":"+config.getPasswordValidityInDays());
		SystemUser mobile = findByMobile(user.getMobile(),user.getDomain(),user.getUserType());
		SystemUser email = findByEmail(user.getEmail(),user.getDomain(),user.getUserType());
		
		if(mobile != null || email != null) {
			logger.info("User Already exists with given mobile number or email");
			return null;
		}
		Calendar expiry = Calendar.getInstance(TimeZone.getTimeZone(user.getTimeZone()));
		user.setCreatedOn(new Date(expiry.getTimeInMillis()));
		expiry.add(Calendar.DATE, config.getPasswordValidityInDays());
		user.setPasswordValidityDays(config.getPasswordValidityInDays());
		user.setPasswordExpiry(new Date(expiry.getTimeInMillis()));
		return mongoService.insert(user);
	}

	@Override
	public SystemUser findById(String id) {
		Query query = new Query(Criteria.where("_id").is(id));
		return mongoService.findOne(query, SystemUser.class);
	}

	
	@Override
	public SystemUser findByMobile(String mobile, String domain, int userType) {
		Query query = new Query(Criteria.where("mobile").is(mobile).and("domain").is(domain).and("userType").is(userType));
		
		return mongoService.findOne(query, SystemUser.class);
	}
	
	@Override
	public SystemUser findByEmail(String email, String domain, int userType) {
		Query query = new Query(Criteria.where("email").is(email).and("domain").is(domain).and("userType").is(userType));
		return mongoService.findOne(query, SystemUser.class);
	}

	@Override
	public List<SystemUser> findAll() {
		
		return mongoService.findAll(SystemUser.class);
	}

	@Override
	public long getUserCount() {
		
		return mongoService.count(new BasicQuery("{}") , SystemUser.class);
	}

	@Override
	public SystemUser findByIdentity(String identity, String domain, int userType) {
		String queryString = String.format(
				"{ "
				+ "$or : "
						+ "["
						+ "{username : \"%s\"},"
						+ "{ mobile : \"%s\"},"
						+ "{email : \"%s\"},"
						+ "{externalReferenceId : \"%s\"}"
						+ "], "
				+ "$and : ["
						+ "{domain : \"%s\"}, "
						+ "{userType : %d}"
						+ "]"
				+ "}",
				identity,identity,identity, identity,domain, userType);
		
		logger.info(queryString);
		Query query = new BasicQuery(queryString);
		//Query query = new Query(Criteria.where("username").is(identity).and("domain").is(domain).and("userType").is(userType));
		SystemUser selected = mongoService.findOne(query, SystemUser.class);
		if(selected != null) {
			return selected;
		}else {
			logger.info("User not found :" + identity);
			return null;
		}
	}


}
