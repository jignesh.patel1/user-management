package com.iwant.userservice.services.common;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iwant.userservice.data.dao.SequenceDao;
import com.iwant.userservice.data.models.AreaModel;
import com.iwant.userservice.data.models.CountryModel;
import com.iwant.userservice.data.models.SequenceId;
import com.iwant.userservice.data.repo.AreaRepo;
import com.iwant.userservice.data.repo.SequenceRepo;

@Service
public class CommonServices {

	private static final Logger logger = LoggerFactory.getLogger(CommonServices.class);
	@Autowired
	AreaRepo areaRepo;
	
	@Autowired
	private SequenceRepo sequenceManager;
	
	@Autowired
	MongoOperations dbService;
	
	private static final String AREA_SEQ_KEY = "AREA";
 
	
	@Transactional
	public AreaModel createNewArea(AreaModel area) {
		logger.trace("CommonServices.createNewArea");
		
		if(areaRepo.count()==0) {
			SequenceId seqId = createNewSequence(AREA_SEQ_KEY,area.getDomain());
			if(seqId != null && seqId.getSeq() !=0) {
				logger.error("Error while creating Sequence:"+AREA_SEQ_KEY);
				return null;
			}
		}
		area.setAreaId(getNextSequence(AREA_SEQ_KEY,area.getDomain()));
		return areaRepo.save(area);

	}
	public AreaModel updateArea(AreaModel area) {
		logger.trace("CommonServices.updateArea");
		
		if(areaRepo.count()==0) {
			SequenceId seqId = createNewSequence(AREA_SEQ_KEY,area.getDomain());
			if(seqId != null && seqId.getSeq() !=0) {
				logger.error("Error while creating Sequence:"+AREA_SEQ_KEY);
				return null;
			}
		}
		area.setAreaId(getNextSequence(AREA_SEQ_KEY,area.getDomain()));
		return areaRepo.save(area);

	}
	
	public long getNextSequence(String key,String domain) {
		String id = domain+"."+key;
		if(sequenceManager.existsById(id)){
			Optional<SequenceId> result = sequenceManager.findById(id);
			if(result.isPresent()) {
				SequenceId seq = result.get();
				long current = seq.getSeq()+1;
				seq.setSeq(current);
				return sequenceManager.save(seq).getSeq();
			}else {
				logger.error("Sequence["+key+"] exists but record not found");
				return -301;
			}
		}else {
			logger.error("Sequence not found:"+key);
			return -404;
		}
		
	}
	public SequenceId createNewSequence(String key,String domain) {
		String id = domain+"."+key;
		logger.info("Creting new Sequence:"+id);
		if(!sequenceManager.existsById(id)){
			SequenceId newSeq = new SequenceId();
			newSeq.setId(id);
			newSeq.setSeq(0);
			return sequenceManager.save(newSeq);
		}else {
			logger.info("sequence already exists:"+id);
			return null;
		}
	}
	
}
