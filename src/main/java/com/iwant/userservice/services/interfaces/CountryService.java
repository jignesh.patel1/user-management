package com.iwant.userservice.services.interfaces;

import java.util.List;

import com.iwant.userservice.data.models.CountryModel;

public interface CountryService {

	public CountryModel createNewCountry(CountryModel input);
	public boolean findAndDeleteById(String id);
	public boolean findAndDeleteByCode(int code);
	
	public CountryModel findAndUpdate(CountryModel input);
	public CountryModel findOneById(String id);
	public CountryModel findOneByCode(int code);
	public List<CountryModel> findAllByDomain(String domain);
	
}
