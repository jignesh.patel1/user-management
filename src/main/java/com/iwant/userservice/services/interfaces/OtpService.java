package com.iwant.userservice.services.interfaces;

import com.iwant.userservice.data.models.security.OtpModel;
import com.iwant.userservice.wrapper.OtpResponse;
import com.iwant.userservice.wrapper.OtpValidation;

public interface OtpService {
	
	public OtpResponse sendOtp(String domain,String transactionInfo,String mcc,String mobile,String email,Object custom);
	public OtpValidation validateOtp(String transactionId,String otp, String identifier,String domain);
	public OtpResponse resendOtp(String domain,String transactionInfo,String mcc,String mobile,String email,Object custom);
	
}
