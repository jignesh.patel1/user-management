package com.iwant.userservice.services.interfaces;

import java.util.List;

import com.iwant.userservice.data.models.security.SystemUser;

public interface SystemUserService {
	
	public SystemUser create(SystemUser user);
	
	public SystemUser findById(String id);
	public SystemUser findByEmail(String mobile, String domain, int userType);
	public SystemUser findByMobile(String mobile, String domain, int userType);
	public SystemUser findByIdentity(String identity,String domain, int userType);
	public List<SystemUser> findAll();
	public long getUserCount();
}
