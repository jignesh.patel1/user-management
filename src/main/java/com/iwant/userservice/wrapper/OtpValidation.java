package com.iwant.userservice.wrapper;

public class OtpValidation {

	private String domain;
	private String transactionCode;
	private String username;
	private String otpValue;
	private String validationRespose;
	private boolean isValid;
	
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getTransactionCode() {
		return transactionCode;
	}
	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getOtpValue() {
		return otpValue;
	}
	public void setOtpValue(String otpValue) {
		this.otpValue = otpValue;
	}
	public String getValidationRespose() {
		return validationRespose;
	}
	public void setValidationRespose(String validationRespose) {
		this.validationRespose = validationRespose;
	}
	public boolean isValid() {
		return isValid;
	}
	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}
}
